# Se tienen dos arrays uno con el nombre de personas y otro con las edades, 
# se pide generar un hash con el nombre y edad de cada persona 
# (se asume que no existen dos personas con el mismo nombre)

personas = ["Carolina", "Alejandro", "Maria Jesús", "Valentín"]
edades = [32, 28, 41, 19]

# 1. Se pide generar un hash con la información:
personas_hash = Hash[personas.zip(edades)]
print personas_hash
puts

# 2. Crear un método que reciba el hash y devuelva el promedio de las edades del hash pasado como argumento.
def promedio_edades(hash)
  suma = 0
  hash.each_value { |value| suma += value}
  return suma / hash.length.to_f
end

puts promedio_edades(personas_hash)

# Extra: Mostrar edad de persona:
def mostrar_edad(persona, hash)
  hash.each { |key, value| return value if key == persona }
end
puts mostrar_edad('Valentín', personas_hash)