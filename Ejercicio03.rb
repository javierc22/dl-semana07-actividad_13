# Dado el Hash:
h = {x: 1, y:2}

# 1. Agregar el string z con el valor 3.
h[:z] = 3
print h
puts

# 2. Cambiar el valor de x por 5.
h[:x] = 5
print h
puts
# 3. Eliminar la clave y.
h.delete(:y)
print h
puts

# 4. Si el hash tiene una clave llamada z mostrar en pantalla "yeeah".
h.each_key { |key| puts 'yeeah' if key == :z}

# 5.Invertir el diccionario de forma que los valores sean las llaves y las llaves los valores
h2 = h.invert
print h2
