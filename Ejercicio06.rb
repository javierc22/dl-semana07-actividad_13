# Escribir un hash con el menu de un restaurant, la llave es el nombre del plato y el valor es el precio de este.
restaurant_menu = { "Ramen" => 3, "Dal Makhani" => 4, "Coffee" => 2 }

# 1. Obtener el plato mas caro.
def most_expensive_dish(menu)
  menu.select { |_key, value| value == menu.values.max }
end

puts most_expensive_dish(restaurant_menu)

# 2. Obtener el plato mas barato.
def cheapest_dish(menu)
  menu.select { |_key, value| value == menu.values.min }
end

puts cheapest_dish(restaurant_menu)
# 3. Sacar el promedio del valor de los platos.
def average(menu)
  menu.values.sum / menu.length.to_f
end

puts average(restaurant_menu)

# 4. Crear un arreglo con solo los nombres de los platos.
def array_names_dishes(menu)
  names = []
  menu.each_key { |key| names.push(key) }
  return names
end

print array_names_dishes(restaurant_menu)
puts

# 5. Crear un arreglo con solo los valores de los platos.
def array_prices_dishes(menu)
  prices = []
  menu.each_value { |value| prices.push(value)}
  return prices
end

print array_prices_dishes(restaurant_menu)
puts

# 6. Modificar el hash y agregar el IVA a los valores de los platos (multiplicar por 1.19).
def add_iva(menu)
  menu.each { |key, value| menu[key] = value * 1.19 }
end

print add_iva(restaurant_menu)
puts

# 7. Dar descuento del 20% para los platos que tengan un nombre de más 1 una palabra.
def add_discount(menu)
  menu.each { |key, value| menu[key] = value - (value * 0.20) if key.include? ' ' }
end

print add_discount(restaurant_menu)