# ------------------- Ejercicio 2 ---------------------- #
productos = {'bebida' => 850, 'chocolate' => 1200, 'galletas' => 900, 'leche' => 750}

productos.each { |producto, _valor| puts producto }

# 1. Se quiere agregar un nuevo producto al hash:
productos['cereal'] = 2200
print productos
puts

# 2. Se quiere actualizar el precio de la bebida:
productos['bebida'] = 2000
print productos
puts

# 3. Corrige la instrucción para actualizar el valor del producto existente.
  # a) Convertir el hash en un array y guardarlo en una nueva variable:
  array_productos = productos.to_a
  print array_productos
  puts

  # b) Eliminar el producto 'galletas' del hash:
  productos.delete('galletas')
  print productos
  puts