# Se tiene un hash con el inventario de un negocio de computadores.
inventario = { Notebooks: 4, PC_Escritorio: 6, Routers: 10, Impresoras: 6}

# --------------------- Main menu --------------------------- #
option = 0
while option != 7
  puts
  puts '#------ Escoge una opción: ------ #'
  puts 'Opcion 1: Agregar un item.-'
  puts 'Opcion 2: Eliminar un item.-'
  puts 'Opcion 3: Actualizar.-'
  puts 'Opcion 4: Ver Stock total.-'
  puts 'Opcion 5: Ver item con mayor Stock.-'
  puts 'Opcion 6: Ver existencia de un item.-'
  puts 'Opcion 7: Salir.-'

  option = 0
  while option <= 0 || option > 7
    puts 'Opción:'
    option = gets.chomp.to_i
    puts
  end

  # --------------------- Option 1 --------------------------- #
  def add_item(inventory)
    puts "Ingrese nuevo item. \'nombre_item, stock\':"
    new_item = gets.chomp.to_s.split(', ')
    inventory[new_item[0].to_sym] = new_item[1].to_i
    puts inventory
  end

  # --------------------- Option 2 --------------------------- #
  def delete_item(inventory)
    puts 'Ingrese Item a eliminar:'
    item_delete = gets.chomp.to_sym
    inventory.delete(item_delete)
    puts inventory
    puts
  end

  # --------------------- Option 3 --------------------------- #
  def update_item(inventory)
    puts "Ingrese item a actualizar \'Item, cantidad\':"
    itemUpdate = gets.chomp.to_s.split(', ')
    inventory[itemUpdate[0].to_sym] = itemUpdate[1].to_i
    puts inventory
  end

  # --------------------- Option 4 --------------------------- #
  def stock_total(inventory)
    stockTotal = inventory.values.sum
    puts "Stock Total: #{stockTotal}"
  end

  # --------------------- Option 5 --------------------------- #
  def item_greater_stock(inventory)
    major = inventory.select { |_key, value| value == inventory.values.max}
    puts "Item con mayor stock: #{major}"
  end

  # --------------------- Option 6 --------------------------- #
  def item_existence(inventory)
    puts 'Ingrese item a consultar:'
    item_search = gets.chomp.to_sym
    search = inventory.has_key?(item_search)

    if search
      puts 'Item existe!'
    else
      puts 'Item no existe!'
    end
    puts
  end

  # --------------------- Option 7 --------------------------- #
  def exit
    puts 'Salir!'
    exit
  end

  case option
  when 1 then add_item(inventario)
  when 2 then delete_item(inventario)
  when 3 then update_item(inventario)
  when 4 then stock_total(inventario)
  when 5 then item_greater_stock(inventario)
  when 6 then item_existence(inventario)
  when 7 then exit
  end
end
