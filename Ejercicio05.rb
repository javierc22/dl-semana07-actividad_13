# Dados los siguientes array:
meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo']
ventas = [2000, 3000, 1000, 5000, 4000]

# Generar un hash que contenga los meses como llave y las ventas como valor:
hash_1 = Hash[meses.zip(ventas)]
print hash_1 # => {"Enero"=>2000, "Febrero"=>3000, "Marzo"=>1000, "Abril"=>5000, "Mayo"=>4000}
puts

# En base al hash generado:
# 1. Invertir las llaves y los valores del hash.
hash_invert = hash_1.invert
print hash_invert # => {2000=>"Enero", 3000=>"Febrero", 1000=>"Marzo", 5000=>"Abril", 4000=>"Mayo"}
puts

# 2. Obtener el mes mayor cantidad de ventas (a partir del hash invertido.)
valor_max = hash_invert.max
print valor_max # => [5000, "Abril"]

# Nota: 
# valor_max = hash_invert.max => [5000, "Abril"]
# valor_max = hash_invert.max.first => 5000
# valor_max = hash_invert.max.last => 'Abril'
